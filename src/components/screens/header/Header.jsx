import Shop from '../shop/Shop'
const Header = (props) => {
  return (
    <>
      <header>
        <div className="nav">
          <ul className="navigation">
            <li>
              <a
                href="#cards"
                className={`nav__link`}
              >
                Магазин
              </a>
            </li>
            <li>
              <a href="#delivery" className="nav__link">
                Доставка и оплата
              </a>
            </li>
            <li>
              <a href="#advantages" className="nav__link">
                Наши преимущества
              </a>
            </li>
            <li>
              <a href="#contacts" className="nav__link">
                Контакты
              </a>
            </li>
          </ul>
          <div className="logo">
            <a href="#header">
              <img src="img/logo2.jpg" alt="logo" />
            </a>
          </div>
        </div>
        <div className="socials-block">
          <div className="socials-block_number">+7 928 264-50-32</div>
          <ul className="socials-block_socials">
            <a href="#!">
              <li>
                <img src="img/socials/header/facebook.svg" alt="facebook" />
              </li>
            </a>
            <a href="#!">
              <li>
                <img src="img/socials/header/instagram.svg" alt="inst" />
              </li>
            </a>

            <a href="#!">
              <li>
                <img src="img/socials/header/vk.svg" alt="vk" />
              </li>
            </a>
            <a href="#!">
              <li>
                <img src="img/socials/header/youtube.svg" alt="youtube" />
              </li>
            </a>
          </ul>
        </div>
        {props.orders.length ? (
          <Shop modal={props.modal} count={props.count}/>
        ) : (
          ""
        )}
      </header>
      <div className="pos" id="header"></div>
    </>
  );
};

export default Header;
