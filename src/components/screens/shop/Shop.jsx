import React, { useContext } from "react";
import { GrShop } from "react-icons/gr";
import "./Shop.css";
import { OrdersContext } from "../../context/Context";

const Shop = (props) => {
  const data = useContext(OrdersContext);

  return (
    <div className="shop" onClick={props.modal}>
      <GrShop className="shop-btn" />
      <div className="shop-count">{props.count}</div>
    </div>
  );
};

export default Shop;
