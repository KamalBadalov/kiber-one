import React, { useContext, useEffect, useRef, useState } from "react";
import classes from "./SendForm.module.css";
import emailjs from "@emailjs/browser";
import FormInput from "./FormInput";
import { OrdersContext } from "../../../../context/Context";

const SendForm = (props) => {
  const form = useRef();
  const [message, setMessage] = useState({ name: "", email: "", number: "" });
  const [order, setOrder] = useState([]);
  const { hide, setModalOrder } = useContext(OrdersContext);

  useEffect(() => {
    setOrder(
      props.orders.map((el) => {
        return "Название: " + el.title + "; Количество: " + el.count + "шт.";
      })
    );
  }, [props.orders]);

  const sendEmail = (e) => {
    e.preventDefault();
    const stringOrder = order.join("\n");
    const templateParams = {
      name: message.name,
      email: message.email,
      number: message.number,
      order: stringOrder,
      sum: props.sum,
    };
    emailjs
      .send(
        "service_v9brtx6",
        "template_hvtz6yn",
        templateParams,
        "Wn_eJ1-itKhLV7J8y"
      )
      .then(
        function (response) {
          console.log("SUCCESS!", response.status, response.text);
        },
        function (error) {
          alert("FAILED...", error);
        }
      );
    props.setOrders([]);
    props.setShopModal(false);
    hide();
    setTimeout(() => {
      setModalOrder(true);
    }, 1000);
  };

  return (
    <>
      <form ref={form} onSubmit={sendEmail} className={classes.form}>
        <FormInput
          type={"text"}
          title={"Ваше имя"}
          value={message.name}
          setMessage={(e) => setMessage({ ...message, name: e.target.value })}
        />
        <FormInput
          type={"email"}
          title={"Ваш Email"}
          value={message.email}
          setMessage={(e) => setMessage({ ...message, email: e.target.value })}
        />
        <FormInput
          type={"tel"}
          title={"Ваш телефон"}
          value={message.number}
          setMessage={(e) => setMessage({ ...message, number: e.target.value })}
        />
        <div className={classes.result}>Сумма: {props.sum} к.</div>
        <input
          type="submit"
          value="Оформить заказ"
          className={classes.resultBtn}
        />
      </form>
    </>
  );
};

export default SendForm;
