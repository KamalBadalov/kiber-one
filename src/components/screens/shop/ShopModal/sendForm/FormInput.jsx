import React from "react";
import classes from "./SendForm.module.css";

const FormInput = (props) => {
  return (
    <div className={classes.orderForm}>
      <div className={classes.title}>{props.title}</div>
      <input
        className={classes.input}
        type={props.type}
        onChange={props.setMessage}
        value={props.value}
        required
      />
    </div>
  );
};

export default FormInput;
