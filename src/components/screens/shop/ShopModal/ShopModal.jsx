import React, { useContext } from "react";
import classes from "./ShopModal.module.css";
import Order from "../Order/Order";
import SendForm from "./sendForm/SendForm";
import { OrdersContext } from "../../../context/Context";


const ShopModal = (props) => {
  const rootClasses = [classes.modal];
  const { hide } = useContext(OrdersContext);

  if (props.shopModal) {
    rootClasses.push(classes.active);
  }

  return (
    <div
      className={rootClasses.join(" ")}
      style={{ paddingTop: `${props.orders.length * 65}px` }}
      onClick={() => {
        hide();
        props.setShopModal(false);
      }}
    >
      <button onClick={hide()} className={classes.close}>
        <img src="/img/close2.svg" alt="close" />
      </button>
      <div
        className={classes.content}
        style={{ marginTop: `${props.orders.length * 25}px` }}
        onClick={(e) => e.stopPropagation()}
      >
        <h1 className={classes.title}>Ваш заказ</h1>
        <div className={classes.orders}>
          {props.orders.length
            ? props.orders.map((card) => (
                <Order
                  card={card}
                  key={card.id}
                  remove={props.remove}
                  inc={props.inc}
                  dec={props.dec}
                  orders={props.orders}
                />
              ))
            : props.setShopModal(false)}
        </div>
        <div className={classes.sum}>Сумма: {props.sum} к.</div>
        <SendForm sum={props.sum} orders={props.orders} setOrders={props.setOrders} setShopModal={props.setShopModal}/>
      </div>
    </div>
  );
};

export default ShopModal;
