import React from "react";
import classes from "./Order.module.css";

const Order = (props) => {
  const order = props.orders.find((el) => el.id === props.card.id);

  return (
    <div className={classes.order}>
      <div className={classes.img}>
        <img src={"img/cards/" + props.card.img} alt="order" />
      </div>
      <div className={classes.title}>{props.card.title}</div>
      <div className={classes.count}>
        <button
          onClick={() => {
            props.dec(order);
          }}
          className={classes.countBtn}
        >
          <img src="img/minus.svg" alt="minus" className={classes.countImg} />
        </button>
        <span className={classes.countNum}>{order.count}</span>
        <button
          onClick={() => {
            props.inc(order);
          }}
          className={classes.countBtn}
        >
          <img src="img/plus.svg" alt="plus" className={classes.countImg} />
        </button>
      </div>
      <div className={classes.amount}>{props.card.price * order.count} к.</div>
      <div className={classes.del}>
        <button
          className={classes.remove}
          onClick={() => props.remove(order)}
        >
          <img src="img/remove.svg" alt="remove" />
        </button>
      </div>
    </div>
  );
};

export default Order;
