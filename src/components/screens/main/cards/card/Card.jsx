import React from "react";
import "./Card.css";
import { useInView } from "react-intersection-observer";
const Card = (props) => {
  const { ref, inView } = useInView({
    threshold: 0.5,
    triggerOnce: true,
  });

  return (
    <div className="card" ref={ref} onClick={props.showCard}>
      {inView ? (
        <img
          src={"img/cards/" + props.card.img}
          alt="card_img"
          className="card_img"
          onMouseOver={(e) =>
            (e.currentTarget.src = "img/cards/" + props.card.img2)
          }
          onMouseOut={(e) =>
            (e.currentTarget.src = "img/cards/" + props.card.img)
          }
        />
      ) : (
        <div className="loader"></div>
      )}
      <h2>{props.card.title}</h2>
      <h3>{props.card.desc}</h3>
      <p>
        {props.card.price}
        <span style={{ marginLeft: "6px" }}>к.</span>
      </p>
    </div>
  );
};

export default Card;
