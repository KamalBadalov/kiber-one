import React from "react";
import Card from "./card/Card";
import "./Cards.css";

const Cards = (props) => {
  return (
    <div className="container">
      <div className="cards section">
        {props.cards.map((card) => (
          <Card
            card={card}
            key={card.id}
            onAdd={props.onAdd}
            showCard={() => props.showCard(card)}
          />
        ))}
        <div className="pos" id="delivery"></div>
      </div>
    </div>
  );
};

export default Cards;
