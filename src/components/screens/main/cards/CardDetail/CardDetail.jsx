import React, { useContext } from "react";
import classes from "./CardDetail.module.css";
import {
  CarouselProvider,
  Slider,
  Slide,
  ButtonBack,
  ButtonNext,
  Dot,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import "./CardDetail.css";
import { OrdersContext } from "../../../../context/Context";

const CardDetail = (props) => {
  const rootClasses = [classes.cardDetail];
  const { hide } = useContext(OrdersContext);

  if (props.detail) {
    rootClasses.push(classes.active);
  }

  const createOrder = () => {
    const newOrder = {
      ...props.card,
      count: 1,
    };
    props.create(newOrder);
  };
  return (
    <div
      className={rootClasses.join(" ")}
      onClick={() => {
        hide();
        props.setDetail(false);
      }}
    >
      <div className={classes.container} onClick={(e) => e.stopPropagation()}>
        <button
          onClick={() => {
            hide();
            props.setDetail(false);
          }}
          className={classes.close}
        >
          <img src="/img/close.svg" alt="close" />
        </button>

        <div className={classes.content}>
          <div className={classes.slider}>
            <CarouselProvider
              naturalSlideWidth={100}
              naturalSlideHeight={125}
              totalSlides={props.cardsImg[props.card.id - 1].length}
              className={classes.carousel}
              infinite={true}
            >
              <div className={classes.photo}>
                {props.cardsImg[props.card.id - 1].length !== 1 ? (
                  <ButtonBack
                    className={[classes.back, classes.arrows].join(" ")}
                  >
                    <img
                      src="/img/back.svg"
                      alt="back"
                      style={{ height: "12px", width: "12px" }}
                    />
                  </ButtonBack>
                ) : (
                  ""
                )}
                <Slider className={classes.slide}>
                  {props.cardsImg[props.card.id - 1].map((slide, index) => (
                    <Slide index={index} key={slide.id}>
                      <img
                        loading="lazy"
                        src={"/img/cards/" + slide}
                        alt="slide"
                        className={classes.slideImg}
                      />
                    </Slide>
                  ))}
                </Slider>
                {props.cardsImg[props.card.id - 1].length !== 1 ? (
                  <ButtonNext
                    className={[classes.next, classes.arrows].join(" ")}
                  >
                    <img
                      src="/img/next.svg"
                      alt="next"
                      style={{ height: "12px", width: "12px" }}
                    />
                  </ButtonNext>
                ) : (
                  ""
                )}
              </div>

              {props.cardsImg[props.card.id - 1].length !== 1 ? (
                <div className={classes.dotGroup}>
                  {props.cardsImg[props.card.id - 1].map((dot, index) => (
                    <Dot slide={index} className={classes.dot} key={dot.id}>
                      <img
                        src={"img/cards/" + dot}
                        alt="slide"
                        className={classes.pagination}
                      />
                    </Dot>
                  ))}
                </div>
              ) : (
                ""
              )}
            </CarouselProvider>
          </div>

          <div className={classes.text}>
            <h1 className={classes.title}>{props.card.title}</h1>
            <h2 className={classes.price}>
              {props.card.price} <span style={{ marginLeft: "6px" }}>к.</span>
            </h2>
            <button
              className={classes.btn}
              onClick={createOrder}
            >
              Добавить в корзину
            </button>
            <p className={classes.desc}>{props.card.desc}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardDetail;
