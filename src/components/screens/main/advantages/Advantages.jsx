import React from "react";
import "./Advantages.css";
import AdvantagesCard from "./AdvantagesCard";
const Advantages = () => {
  return (
    <div className="container">
      <div className="advantages" >
        <h1>Наши преимущества</h1>
        <div className="advantages__cards">
        <AdvantagesCard
          img={"img/advantages/star.svg"}
          title={"Уникальность"}
          paragraph={"Только для резидентов КиберШколы KIBERone"}
        />
        <AdvantagesCard
          img={"img/advantages/virtual.svg"}
          title={"Современность"}
          paragraph={"Только модные и технологичные аксессуары"}
        />
        <AdvantagesCard
          img={"img/advantages/money.svg"}
          title={"За кибероны"}
          paragraph={"Продается за уникальную внутреннюю валюту"}
        />
        </div>
      </div>
    </div>
  );
};

export default Advantages;
