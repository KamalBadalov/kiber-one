import React from "react";

const AdvantagesCard = (props) => {
  return (
    <div>
      <img src={props.img} alt="img" className="advantages__img"/>
      <div className="advantages__title">{props.title}</div>
      <div className="advantages__paragraph">{props.paragraph}</div>
    </div>
  );
};

export default AdvantagesCard;
