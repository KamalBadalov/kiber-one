import React from "react";

const Promo = () => {
  return (
    <div className="promo">
      <div className="container">
        <p>
          В Кибермаркете ты можешь найти практически все что тебе нужно начиная
          c ручки и рукзака в стильном дизайне от KiberOne и заканчивая Sony
          Playstation 5
        </p>
      </div>
    </div>
  );
};

export default Promo;
