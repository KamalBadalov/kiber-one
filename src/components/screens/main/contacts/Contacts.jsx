import React from "react";
import classes from "./Contacts.module.css";

const Contacts = () => {
  return (
    <div className="container">
      <div className="pos2" id="contacts"></div>
      <div className={classes.contacts}>
        <h1>Присоединяйтесь к IT-школе будущего Kiber One</h1>
        <p className={classes.subtitle}>
          Все по-взрослому, только на детском языке
        </p>
        <div className={classes.content}>
          <div className={classes.info}>
            <div className={classes.numAndEmail}>
              <p>+7 928 264-50-32</p>
              <p>kiberone.essentuki@mail.ru</p>
            </div>
            <div>
              <p className={classes.address}>
                Ессентуки, Пятигорская 118А, 3 этаж 301 кабинет
              </p>
              <div className={classes.block}>
                <ul className={classes.socials}>
                  <li>
                    <a href="#!">
                      <img
                        src="img/socials/footer/facebook.svg"
                        alt="facebook"
                      />
                    </a>
                  </li>
                  <li>
                    <a href="#!">
                      <img src="img/socials/footer/instagram.svg" alt="inst" />
                    </a>
                  </li>
                  <li>
                    <a href="#!">
                      <img src="img/socials/footer/vk.svg" alt="vk" />
                    </a>
                  </li>
                  <li>
                    <a href="#!">
                      <img src="img/socials/footer/youtube.svg" alt="youtube" />
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className={classes.inputs}>
            <input
              type="text"
              placeholder="Ваш email"
              className={classes.input}
            />
            <input
              type="text"
              placeholder="Ваше имя"
              className={classes.input}
            />
            <textarea
              name=""
              id=""
              placeholder="Комментарий"
              className={classes.input}
            ></textarea>
            <button className={classes.btn}>Отправить</button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contacts;
