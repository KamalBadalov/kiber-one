import { Splide, SplideSlide } from "@splidejs/react-splide";
import React from "react";
import "@splidejs/react-splide/css";
import "./Slider.css";
const Slider = () => {
  return (
    <>
      <Splide
        aria-label="My Favorite Images"
        options={{
          type: "fade",
          rewind: true,
        }}
        className="splide"
      >
        <SplideSlide className="splide__slide">
          <div
            className="splide__slide-img"
            style={{ backgroundImage: "url(img/1.png)" }}
          >
            <span>Магазин КиберПриятностей</span>
            <a href="#cards" className="slide_btn">
              магазин
            </a>
          </div>
        </SplideSlide>
        <SplideSlide className="splide__slide">
          <div
            className="splide__slide-img"
            style={{ backgroundImage: "url(img/2.png)" }}
          >
            <span style={{ width: "700px" }}>
              Отслеживай количество своих киберонов и заказывай то что нравится!
            </span>
            <a href="#cards" className="slide_btn">
              магазин
            </a>
          </div>
        </SplideSlide>
      </Splide>
      <div className="pos2" id="cards"></div>
    </>
  );
};

export default Slider;
