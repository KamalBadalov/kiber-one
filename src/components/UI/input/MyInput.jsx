import React from "react";
import classes from "./MyInput.module.css";
const MyInput = (props) => {


  return (
    <div className={classes.form}>
      <div className={classes.title}>{props.title}</div>
      <input
        type="text"
        className={classes.input}
        onChange={(e) => props.setValue(e.target.value)}
      />
    </div>
  );
};

export default MyInput;
