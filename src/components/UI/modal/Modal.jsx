import React, { useEffect, useState } from "react";
import classes from "./Modal.module.css";
const Modal = ({ children, visible, setVisible }) => {
  const rootClasses = [classes.modal];
  const [count, setCount] = useState(5);
  if (visible) {
    rootClasses.push(classes.active);
  }
  useEffect(() => {
    setTimeout(() => {
      setCount(count - 1);
    }, 1000);
  }, [count]);
  return (
    <div className={rootClasses.join(" ")} onClick={() => setVisible(false)}>
      <div className={classes.content} onClick={(e) => e.stopPropagation()}>
        {children}
        <div>Закроется автоматически через: {count}</div>
      </div>
    </div>
  );
};

export default Modal;
