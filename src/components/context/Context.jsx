import React, { createContext, useState } from "react";
export const OrdersContext = createContext();
const Context = ({ children }) => {
  const [count, setCount] = useState(1);
  const hide = () => {
    document.body.style.overflow = "visible";
  };
  const [modalOrder, setModalOrder] = useState(false)
 
  return (
    <OrdersContext.Provider
      value={{ count, setCount, hide, modalOrder, setModalOrder }}
    >
      {children}
    </OrdersContext.Provider>
  );
};

export default Context;
