import "./css/reset.css";
import "./App.css";
import { useContext, useState } from "react";
import { OrdersContext } from "./components/context/Context";
import Header from "./components/screens/header/Header";
import ShopModal from "./components/screens/shop/ShopModal/ShopModal";
import Promo from "./components/screens/main/promo/Promo";
import Cards from "./components/screens/main/cards/Cards";
import Delivery from "./components/screens/main/delivery/Delivery";
import CardDetail from "./components/screens/main/cards/CardDetail/CardDetail";
import Advantages from "./components/screens/main/advantages/Advantages";
import Contacts from "./components/screens/main/contacts/Contacts";
import Footer from "./components/screens/footer/Footer";
import Slider from "./components/UI/slider/Slider";
import Modal from "./components/UI/modal/Modal";

function App() {
  const cards = [
    {
      id: 1,
      title: "Рюкзак бирюзовый Kiber One.",
      img: "1/1.jpg",
      img2: "1/2.jpg",
      desc: "Модный вместительный рюкзак IT-специалиста.",
      price: 150,
    },
    {
      id: 2,
      title: "Вместительная флешка",
      img: "2/1.jpg",
      img2: "2/2.jpg",
      desc: "Флешка в стиле Minecraft для крайне секретной информации",
      price: 50,
    },
    {
      id: 3,
      title: "Силиконовый браслет",
      img: "3/1.jpg",
      img2: "3/1.jpg",
      desc: "Желтый браслет в поддержку любимой школы",
      price: 15,
    },
    {
      id: 4,
      title: "Черная бейсболка",
      img: "4/1.jpg",
      img2: "4/2.jpg",
      desc: "Крутая бейсболка для настоящих хакеров",
      price: 80,
    },
    {
      id: 5,
      title: "Фирменная футболка",
      img: "5/1.jpg",
      img2: "5/2.jpg",
      desc: "Яркая хлопчатобумажная футболка с фирменной сиволикой",
      price: 100,
    },
    {
      id: 6,
      title: "Носки айтишника",
      img: "6/1.jpg",
      img2: "6/1.jpg",
      desc: "Пара модных носков под кроссовки ил кеды",
      price: 30,
    },
    {
      id: 7,
      title: "Шариковая ручка",
      img: "7/1.jpg",
      img2: "7/1.jpg",
      desc: "Синяя шариковая ручка 'Тыпрограммист'",
      price: 10,
    },
    {
      id: 8,
      title: "Набор наклеек",
      img: "8/1.jpg",
      img2: "8/1.jpg",
      desc: "Технологичные наклейки в виде микросхем",
      price: 10,
    },
  ];
  const cardsImg = [
    ["1/1.jpg", "1/2.jpg", "1/3.jpg", "1/4.jpg"],
    ["2/1.jpg", "2/2.jpg"],
    ["3/1.jpg"],
    ["4/1.jpg", "4/2.jpg"],
    ["5/1.jpg", "5/2.jpg", "5/3.jpg"],
    ["6/1.jpg"],
    ["7/1.jpg"],
    ["8/1.jpg"],
  ];
  const data = useContext(OrdersContext);
  // карточка товара
  const [detail, setDetail] = useState(false);
  const [fullItem, setFullItem] = useState("");
  const showCard = (card) => {
    setFullItem(card);
    setDetail(true);
    document.body.style.overflow = "hidden";
  };
  // карточка товара
  // корзина

  const [orders, setOrders] = useState([]);
  const [count, setCount] = useState(0);
  // const [modalCount, setModalCount] = useState(10);
  const [shopModal, setShopModal] = useState(false);
  const [sum, setSum] = useState(0);

  if (data.modalOrder) {
    data.setModalOrder(true);
    setTimeout(() => {
      data.setModalOrder(false);
    }, 5000);
  }
  const addToOrder = (newOrder) => {
    let isInCart = false;
    orders.forEach((el) => {
      if (el.id === newOrder.id) {
        isInCart = true;
      }
    });
    if (!isInCart) {
      setOrders([newOrder, ...orders]);
      setSum(sum + newOrder.price);
      setCount(count + 1);
    }
    if (isInCart) {
      setOrders((prev) =>
        prev.find((item) => item.id === newOrder.id)
          ? prev.map((item) => ({
              ...item,
              count: item.id === newOrder.id ? item.count + 1 : item.count,
            }))
          : [...prev, newOrder]
      );
      setSum(sum + newOrder.price);
    }
    setShopModal(true);
    setDetail(false);
  };

  const removeOrder = (order) => {
    if (orders.length === 1) data.hide();
    setOrders(orders.filter((p) => p.id !== order.id));
    setCount(count - order.count)
    if (order.count) {
      setSum(sum - order.price * order.count);
    }
  };

  const inc = (order) => {
    setCount(count + 1);
    setSum(sum + order.price);
    setOrders((prev) =>
      prev.find((item) => item.id === order.id)
        ? prev.map((item) => ({
            ...item,
            count: item.id === order.id ? item.count + 1 : item.count,
          }))
        : [...prev, order]
    );
  };
  const dec = (order) => {
    setCount(count - 1);
    setSum(sum - order.price);
    setOrders((prev) =>
      prev.find((item) => item.id === order.id)
        ? prev.map((item) => ({
            ...item,
            count: item.id === order.id ? item.count - 1 : item.count,
          }))
        : [...prev, order]
    );
    if (order.count === 1) removeOrder(order);
  };

  const modal = () => {
    setShopModal(true);
    document.body.style.overflow = "hidden";
  };
  // корзина

  return (
    <div className="App">
      <Header modal={modal} orders={orders} count={count}/>
      <Slider />
      <Promo />
      <Cards cards={cards} showCard={showCard} />
      <Delivery />
      {detail && (
        <CardDetail
          showCard={showCard}
          card={fullItem}
          detail={detail}
          setDetail={setDetail}
          cardsImg={cardsImg}
          addToOrder={addToOrder}
          create={addToOrder}
        />
      )}
      <Advantages />
      <Contacts />
      <Footer />
      {shopModal ? (
        <ShopModal
          orders={orders}
          setOrders={setOrders}
          modal={modal}
          shopModal={shopModal}
          setShopModal={setShopModal}
          remove={removeOrder}
          inc={inc}
          dec={dec}
          sum={sum}
        />
      ) : (
        ""
      )}
      {data.modalOrder ? (
        <Modal
          visible={data.modalOrder}
          setVisible={data.setModalOrder}
          // count={modalCount}
          // setCount={setModalCount}
        >
          <h1 style={{ color: "green" }}>Заказ оформлен!</h1>
        </Modal>
      ) : (
        ""
      )}
    </div>
  );
}

export default App;
